'use strict';

angular.module('myCV.version', [
  'myCV.version.interpolate-filter',
  'myCV.version.version-directive'
])

.value('version', '0.1');
