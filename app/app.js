'use strict';

// Declare app level module which depends on views, and components
var app = angular.module('myCV', [
    'myCV.viewNotesList',
    'myCV.viewEditNote',
    'myCV.viewNewNote',
    'myCV.viewCV',
    'myCV.viewCert',
    'ngRoute',
    'myCV.version',
    'ui.bootstrap'
]).config(['$locationProvider', '$routeProvider', function ($locationProvider, $routeProvider) {
    //$locationProvider.hashPrefix('!');
    $routeProvider.otherwise({redirectTo: '/'});
}]);

//Plain JavaScript - Footer Element Handling
var footer = document.getElementsByClassName("bottom-left")[0];
window.addEventListener("scroll", function(){
    var winheight= window.innerHeight || (document.documentElement || document.body).clientHeight;
    var docheight = getDocHeight();
    var scrollTop = window.pageYOffset || (document.documentElement || document.body.parentNode || document.body).scrollTop;
    var trackLength = docheight - winheight;
    var pctScrolled = Math.floor(scrollTop/trackLength * 100);
    if(pctScrolled > 90){
        footer.style.display = "none";
    }else {
        if(footer.style.display === "none"){
            footer.style.display = "block";
        }
    }
}, false);

function getDocHeight() {
    var D = document;
    return Math.max(
        D.body.scrollHeight, D.documentElement.scrollHeight,
        D.body.offsetHeight, D.documentElement.offsetHeight,
        D.body.clientHeight, D.documentElement.clientHeight
    )
}








