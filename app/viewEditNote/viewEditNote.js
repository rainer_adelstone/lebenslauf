'use strict';

angular.module('myCV.viewEditNote', ['ngRoute', 'ui.bootstrap'])

    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/note/edit/:id', {
            templateUrl: 'viewEditNote/viewEditNote.html',
            controller: 'ViewEditNoteCtrl'
        });
    }])

    .controller('ViewEditNoteCtrl', function ($scope, $q, $location, $routeParams, NotesService) {
        var id = $routeParams.id;

        function showNote(stored_notes) {
            var deferred = $q.defer();
            deferred.resolve(stored_notes);
            return deferred.promise;
        }

        showNote(NotesService.getNoteById(id)).then(function (output) {
            // success
            $scope.note = output;
        }, function () {
            // error
            onError();
        });

        $scope.updateNote = function () {
            NotesService.updateNote($scope.note.title, $scope.note.body, $scope.note.id);
            $location.path('/notes');
        };

        $scope.deleteNote = function () {
            NotesService.deleteNoteById($scope.note.id);
            $location.path('/notes');
        };

        $scope.goBack = function () {
            $location.path('/notes');
        };

    });

function onError() {
    alert('oops... ein Fehler ist aufgetreten');
}