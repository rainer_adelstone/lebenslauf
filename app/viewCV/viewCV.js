'use strict';

angular.module('myCV.viewCV', ['ngRoute', 'ui.bootstrap'])

    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/', {
            templateUrl: 'viewCV/viewCV.html',
            controller: 'ViewCV'
        });
    }])

    .controller('ViewCV', [function ($scope) {

    }]);