app.factory('NotesService', function ($window, $q) {

    // Private vars & methods

    var srv = {};
    srv._notes = [];

    srv.getStorage = function () {
        var deferred = $q.defer();
        deferred.resolve(JSON.parse($window.localStorage.getItem('cv-notes')));
        return deferred.promise;
    };


    srv.getNotes = function () {
        return srv.getStorage().then(function (stored_notes) {
            if (stored_notes && stored_notes.length > 0) {
                srv._notes = stored_notes;
            }
            return angular.copy(srv._notes);
        });
    };

    srv.getNoteById = function (id) {
        return srv.getStorage().then(function (stored_notes) {
            srv._notes = stored_notes;
            for (var i = 0, n = srv._notes.length; i < n; i++) {
                if (id == srv._notes[i].id) {
                    return angular.copy(srv._notes[i]);
                }
            }
        });
    };

    srv.storeNote = function (title, body, stored_notes) {
        var id = 0;
        if (stored_notes && stored_notes.length > 0) {
            for (var i = 0; i < srv._notes.length; i++) {
                if (srv._notes[i].id > id) {
                    id = srv._notes[i].id;
                }
            }
            srv._notes.push(new Note(id + 1, title, body));
        } else {
            srv._notes.push(new Note(id, title, body));
        }
        $window.localStorage.setItem('cv-notes', JSON.stringify(srv._notes));
    };

    srv.updateNote = function (title, body, id) {
        for (var i = 0; i < srv._notes.length; i++) {
            if (srv._notes[i].id == id) {
                srv._notes[i].title = title;
                srv._notes[i].body = body;
                break;
            }
        }
        $window.localStorage.setItem('cv-notes', JSON.stringify(srv._notes));

    };

    srv.deleteNoteById = function (id) {
            for (var i = 0; i < srv._notes.length; i++) {
                if (srv._notes[i].id == id) {
                    srv._notes.splice(i, 1);
                    break;
                }
            }
            $window.localStorage.setItem('cv-notes', JSON.stringify(srv._notes));
    };

    //Public API

    return {
        getNotes: function () {
            return srv.getNotes();
        },

        storeNote: function (title, body) {
            srv.getStorage().then(function (stored_notes) {
                return srv.storeNote(title, body, stored_notes);
            });
        },

        getNoteById: function (id) {
            return srv.getNoteById(id);
        },

        updateNote: function (title, body, id) {
            return srv.updateNote(title, body, id);
        },

        deleteNoteById: function (id) {
            return srv.deleteNoteById(id);
        }

    };
});

// note Object constructor
function Note(id, title, body) {
    this.id = id;
    this.title = title;
    this.body = body;
}