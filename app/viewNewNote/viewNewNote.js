'use strict';

angular.module('myCV.viewNewNote', ['ngRoute', 'ui.bootstrap'])

    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/notes/new', {
            templateUrl: 'viewNewNote/viewNewNote.html',
            controller: 'ViewNewNoteCtrl'
        });
    }])

    .controller('ViewNewNoteCtrl', function ($scope, $q, $location, NotesService) {

        $scope.addNote = function () {
            NotesService.storeNote($scope.newNote.title, $scope.newNote.body);
            $location.path('/notes');
        };

        $scope.goBack = function () {
            $location.path('/notes');
        };

    });