'use strict';

angular.module('myCV.viewCert', ['ngRoute', 'ui.bootstrap'])

    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/certificates', {
            templateUrl: 'viewCert/viewCert.html',
            controller: 'ViewCert'
        });
    }])

    .controller('ViewCert', function ($scope) {
        //$scope.myInterval = 5000;
        $scope.noWrapSlides = false;
        $scope.active = 0;
        $scope.slides = [
            {id:0, image:'/certificates/devoteam.jpg', text:'Devoteam'},
            {id:1, image:'/certificates/sidion.jpg', text:'Sidion'},
            {id:2, image:'/certificates/rhenus_0.jpg', text:'Rhenus Seite 1'},
            {id:3, image:'/certificates/rhenus_1.jpg', text:'Rhenus Seite 2'}
        ];
    });