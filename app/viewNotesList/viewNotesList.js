'use strict';

angular.module('myCV.viewNotesList', ['ngRoute', 'ui.bootstrap'])

    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/notes', {
            templateUrl: 'viewNotesList/viewNotesList.html',
            controller: 'ViewNotesListCtrl'
        });
    }])

    .controller('ViewNotesListCtrl', function ($scope, $q, $location, NotesService) {

        function showNotesList(stored_notes) {
            var deferred = $q.defer();
            deferred.resolve(stored_notes);
            return deferred.promise;
        }

        showNotesList(NotesService.getNotes()).then(function (output) {
            // success
            $scope.notes = output;
        }, function () {
            // error
            onError();
        });
    });

function onError() {
    alert('oops... ein Fehler ist aufgetreten');
}